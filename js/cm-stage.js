function setupEditors() {
txtAreas = document.querySelectorAll(".editor");

function getGist(entry) {
	/* NOT TESTED END-to-END*/
	/* Supports a range of entry types, including:

		// No username && no filename - FASTEST
		getGist('d039054506c1ab6bafc6');

		// Has username but no filename
		getGist('metasean/d039054506c1ab6bafc6');

		// Has username && filename
		getGist('metasean/d039054506c1ab6bafc6#file-gun-anonymous_put');
		
		// Has username && filename && multiple files
		getGist('dergachev/4033291#file-readme-md');
		
		// Has filename && multiple files - BEST IF MULTIPLE FILES
		getGist('dergachev/4033291#file-readme-md');

		// Is a full url 
		getGist('https://gist.github.com/dergachev/4033291#file-readme-md');
		getGist('gist.github.com/dergachev/4033291#file-readme-md');
	*/
	// input validation
	if (entry === undefined) { return false };

	// global callback variable for Gists
	window.gistCallback = function(response){
		// log the object - FOR TROUBLESHOOTING
		console.log('gist response:', response);
		// get all files associated with the specified gist
		var availableFiles = response.data.files;
		// we'll assign the appropriate file's name to filename
		var filename;
		if (!detail[2]) { 
			// if no file was specified, we'll use the first one listed
			file = Object.keys(availableFiles)[0];
		} 
		else { 
			// if a file was specified, we need to strip it of 'file-'
			var desiredFile = detail[2].substring(5); 
			// then loop of the available files
			for (file in availableFiles) {
				// convert each to a gist url format
				var files_url_name = ((file.toLowerCase()).replace('.', '-'));
				// if the gist url formatted filename matches
				if (files_url_name === desiredFile) {
					// then this is our winner
					filename = file;
				}
			}
		}
		// set and return the actual code!
		editor.setValue(response.data.files[filename].content);
		return response.data.files[filename].content;
	};

	// determine if a full url was provided
	// if break out the necessary parts
	var entry_start = entry.substr(0, 4);
	if (entry_start === 'http' || entry_start === 'gist') {
		entry = entry.split('.com/')[1];
	}

	// split entry into components
	//including username, gist id, & filename  
	var detail = entry.split('/');
	var hash = detail[0].split('#'); // username
	if (detail[1] === undefined) {
		detail[1] = detail[0];
		detail[0] = undefined;
	};
	hash = detail[1].split('#');
	detail[1] = hash[0];             // gist id
	detail[2] = hash[1];             // filename in url format
	
	// !!!     create the retrieval url,      !!!
	// !!! which INCLUDES THE global callback !!!
	var url = "https://api.github.com/gists/" + detail[1] + "?callback=gistCallback";

	// create a DOM element
	var gistScript = document.createElement('script');

	// add the new script file to the DOM element
	gistScript.src = url;

	// append the DOM element; 
	// when returned, makes the appropriate callback
	document.body.appendChild(gistScript);
}

function getFile(filename) {
	/* TODO: CURRENTLY THIS ONLY TAKES LOCAL FILES */
	/* Should only use SSL links! ('cause security!) */

	// input validation
	if (filename === undefined) { return false }; //TODO handle

	// based largely on:
	// https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/Synchronous_and_Asynchronous_Requests#Example_send_a_file_to_the_console_log
	var xhr = new XMLHttpRequest();
	xhr.open("GET", filename, true);
	//xhr.onload = function (e) {

	xhr.overrideMimeType("text/plain");
	xhr.onreadystatechange = function (e) {
		if(xhr.readyState === 4) {
			if(xhr.status === 200 || xhr.status == 0) {
           	editor.setValue(xhr.responseText);
				return xhr.responseText;
			}
			else {
				console.log('ERROR retrieving local file:', xhr.statusText);
			}
		}
	}
	xhr.onerror = function (e) {
		console.log('ERROR retrieving local file:', xhr.statusText);
	}
	xhr.send(null);
};

function selectScript (txtArea) {
	if (txtArea.dataset.gistId !== undefined) {    // using gist
		getGist(txtArea.dataset.gistId);
		return ['gist'];
	};
	if (txtArea.dataset.fileName !== undefined) {  // using file
		getFile(txtArea.dataset.fileName);
		return ['file'];
	};
	if (txtArea.value) {                           // using textarea
		return ['embedded', txtArea.value];
	}
	return '\n'; // if no scripts, return a single empty line
}

/* START ACTUAL LOOP TO GENERATE EACH EDITOR */
	for (i = 0; i < txtAreas.length; i++) {
		selectScript(txtAreas[i]); // start processing possible script sources
		var txtArea = txtAreas[i]; // editor being processed
		var id = txtArea.parentNode.id = txtArea.parentNode.id ||'editor-' + i;
		var thisEditor = '#' + id + ' .editor';
		var scriptDiv; // for scope availability


		CodeMirror.commands.autocomplete = function(cm) {
			cm.showHint({hint: CodeMirror.hint.anyword});
		}

		// http://codemirror.net/doc/manual.html#config
		//var editor = CodeMirror.fromTextArea(document.querySelector(thisEditor), {
		var editor = CodeMirror(function(div) {
			// make div available outside this scope
			scriptDiv = div; 

			// replace the actual node
			txtArea.parentNode.replaceChild(scriptDiv, txtArea);
		}, {
			value: txtArea.value, // default to textarea
			mode: 'javascript',
			styleActiveLine: true,
			matchBrackets: true,
			autoCloseBrackets: true,
			theme: 'dracula',
			smartIndent: true,
			tabSize: 3,
			indentWithTabs: true,
			lineWrapping: true,
			lineNumbers: true,
			firstLineNumber: 0,
			readOnly: false,
			showCursorWhenSelecting: false,
			cursorBlinkRate: 0,
			autofocus: true,
			viewportMargin: Infinity,
			keyMap: 'sublime',
				extraKeys: {"Ctrl-Space": "autocomplete",
								"Alt-Tab": 'autocomplete'},
			/* to load specific scripts, review:
				http://codemirror.net/doc/manual.html#api_doc
				value: 
			*/
		});

		var container = document.createElement('div');
		container.className = 'buttons';

		// append the button container
		scriptDiv.parentNode.appendChild(container);

		var runButton = document.createElement("button");
		runButton.textContent = "Run";
		runButton.className = "run-button";
		runButton.editorInstance = editor;
		runButton.onclick = runCode;
		container.appendChild(runButton);

		var resetButton = document.createElement("button");
		resetButton.textContent = "Reset";
		resetButton.className = "reset-button";
		resetButton.editorInstance = editor;
		resetButton.originalContent = scriptDiv.textContent;
		resetButton.onclick = resetCode;
		container.appendChild(resetButton);

		var outputDiv = document.createElement("div");
		outputDiv.className = "output-div hidden";
		outputDiv.editorInstance = editor;
		var outputHeight = scriptDiv.parentNode.dataset.outputHeight;
		outputDiv.style.height = outputHeight;
		scriptDiv.parentNode.appendChild(outputDiv);
	}
/* STOP ACTUAL LOOP TO GENERATE EACH EDITOR */
}

function runCode(e) {
	var outputDiv = this.parentNode.parentNode.lastChild;
	var ourID = this.parentNode.parentNode.id;
	var getElemStmt = 'var outputDiv = document.getElementById("' + 
							ourID + '").lastChild\n\n';
	var code = getElemStmt + this.editorInstance.getValue();
	var script = document.createElement("script");
	script.textContent = code;
	document.body.appendChild(script);
}

function resetCode(e) {
	this.editorInstance.doc.setValue(this.originalContent);
	var outputDiv = this.parentNode.lastChild;
	outputDiv.innerHTML = "";
	outputDiv.classList.add("hidden");
}

function formatConsole(item) {
	if (typeof(item) === "object") {
			if (item.length) { // item is an array or arraylike object
			var str = "[";
			for (var i = 0; i < item.length; i++) {
				str += format(item[i]);
				if (i < item.length - 1) {
					str += ", ";
				}
			}
			str += "]";
			return str;
		}
		else { // some other kind of object
			var str = "{";
			for (prop in item) {
				str += prop + ": " + formatConsole(item[prop.toString()]) + ", ";
			}
			if (str.length > 2) {
				str = str.slice(0, str.length - 2);
				str += "}";
			}
			return str;
		}
	}
	else {
		return item;
	}
}

function showConsole(args) {
	if (outputDiv.classList.contains("hidden")) {
		outputDiv.classList.remove("hidden");
	}
	var para = document.createElement("p");
	outputDiv.appendChild(para);
	for (var i = 0; i < args.length; i++) {
		para.innerHTML += formatConsole(args[i]);
		para.innerHTML += ' ';
	}

	outputDiv.parentNode.classList.add('log-shown');
} 	

/* `console.browserLog` MUST PROCEED the 
	`console.log = function` expression
	or else it will make infinite calls
*/
console.browserLog = console.log; 

console.log = function() {
	showConsole(arguments);
	console.browserLog.apply(this, arguments);
}

setupEditors();